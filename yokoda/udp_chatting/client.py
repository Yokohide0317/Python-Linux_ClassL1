import socket

M_SIZE = 1024

# ipアドレス。今回は内側で通信するので、localhostでおk。
host = "localhost"

##ここを、相手と同じPortにする必要がある。
##また、他のペアと被らないようにしてね。
port = 8890
serv_address = (host, port)

# ソケットの準備
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    try:
        # messageを送信する
        message = input(">")
        if message != 'end':
            send_len = sock.sendto(message.encode('utf-8'), serv_address)

            # 「message」を受け付ける。
            print('返信待ち。')
            rx_meesage, addr = sock.recvfrom(M_SIZE)
            msg = rx_meesage.decode(encoding='utf-8')
            print("Server:", msg)

        else:
            print('closing socket')
            sock.close()
            print('done')
            break

    except KeyboardInterrupt:
        print('closing socket')
        sock.close()
        print('done')
        break
