import socket
import time

M_SIZE = 1024

# ipアドレスの指定。今回は内側で通信するので、localhostでおk。
host = 'localhost'

##ここを、相手と同じPortにする必要がある。
##また、他のペアと被らないようにしてね。
port = 8890

locaddr = (host, port)

# ソケットの準備
sock = socket.socket(socket.AF_INET, type=socket.SOCK_DGRAM)
print('準備完了！')

# ipアドレスとポートで用意。
sock.bind(locaddr)

while True:
    try :
        # 「message」を受け付ける。
        print('返信待ち')
        message, cli_addr = sock.recvfrom(M_SIZE)
        message = message.decode(encoding='utf-8')
        print("Client:", message)

        # Clientが受信待ちになるまで待つため1秒待つ。
        time.sleep(1)

        # 入力待ち。相手に返信する。
        response = input(">")
        sock.sendto(response.encode(encoding='utf-8'), cli_addr)

    except KeyboardInterrupt:
        print ('\n . . .\n')
        sock.close()
        break
