
##挿入モード
stat1 = "最初に、i を押すと挿入モードになる。"


##カーソル操作
stat2 = "矢印キーで移動し、文字を打ち込めるよ。"



##入力テスト。自分の名前に変更してみて
name = "YourName"



##保存方法
stat3 = "入力終了したら、、"
stat4 = "『esc』 + 「:」 + 「w」 + 「q」を押して、"
stat5 = "最後に 『Enter』を押すと保存完了！"


##コマンド画面に戻ったら、
stat6 = "python3 how_to_vim.py"
stat7 = "を実行してみよう！"




print(name, "さん！これであなたもVimmerですね！")
print("使い方を復習しましょう！\n\n")
print("1,", stat1)
print("2,", stat2, "\n")
print("3,", stat3)
print(stat4)
print(stat5, "\n\n")
print("使い方を忘れたら、\n", stat6, "でもう一回確認できるよ！")


